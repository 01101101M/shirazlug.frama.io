---
title: "جلسه ۵۶"
subtitle: "بحث آزاد"
date: "1395-02-04"
author: "Mahshid, mehdiMj"
draft: false
tags: ['lug talk', 'Contribution']
categories:
    - "sessions"
readmore: true
---
جلسه ۵۶ شیرازلاگ بحث آزاد بود و طی آن تصمیم گرفته شد که یک ریپوزیتوری برای شیرازلاگ تشکیل داده شود و توسط مهندس خزاعی و مهندس جفره و دیگر افراد حاضر در جلسه کمی در مورد مسائل فنی مربوط به آن بحث و گفت و گو شد. 

